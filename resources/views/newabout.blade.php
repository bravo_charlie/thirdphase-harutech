 @extends('layouts.new.app', ['title' => 'About'],['discription'=> ($pageSetting->tagline)],['site_url'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title" 
     style="background-image: url(uploads/about-us/{{$about->banner_image}}); 
      background-size: cover; position: relative;">
  <div class="container">
    <h1 class="page-title">About Us
    </h1>
</div>
 @if(Auth::check())
  <div class="admin-visibility">
     <i class="aboutedit fa fa-pencil" onclick="editbanner()">
     </i>
  </div>
  <div class="modal fade" id="bannerabout" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form action="/home/aboutbanner" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                 </div>
                  <div class="modal-body">
                    <div class="row form-group">
                       <div class="col col-md-3">
                         <label for="banner_image" class=" form-control-label">Banner Image input (1)</label><br>
                         <span class="au-breadcrumb-span">[ Note: Please upload the image size [1920*305] & should be less than 100kb ]</span>
                       </div>
                       <div class="col-12 col-md-9 process">
                         <input type="file" id="banner_image" accept="image/png, image/jpg, image/jpeg" name="banner_image" class="form-control-file">
                         <img src="/uploads/about-us/{{$about->banner_image}}" alt="{{$about->name}}">
                       </div>
                     </div>
                  </div>
                   <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <script>
          function editbanner(){
            $('#bannerabout').modal('show');
          }
          </script>
      @endif
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">About Us</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default" style="position: relative;">
  <div class="container">
    <div class="row row-50 align-items-lg-center justify-content-xl-between">
      <div class="col-lg-6 text-justify">
        <div class="block-xs">
        </div>
        <?php echo ($about->who_are_we)?>
      </div>
      <div class="col-lg-6">
        <div class="box-images box-images-variant-3">
          <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -20,   &quot;smoothness&quot;: 30 }">
            <img src="/uploads/about-us/{{$about->who_are_we_image1}}" alt="" width="470" height="282"/>
          </div>
          <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }">
            <img src="/uploads/about-us/{{$about->who_are_we_image2}}" alt="" width="470" height="282"/>
          </div>
        </div>
      </div>
    </div>
  </div>
  @if(Auth::check())
  <div class="admin-visibility">
     <i class="aboutedit fa fa-pencil" onclick="editaboutus()">
     </i>
  </div>
  <div class="modal fade" id="aboutuschange" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form action="/home/aboutedit" method="POST" enctype="multipart/form-data">
                 @csrf
                <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                       </button>
                      </div>
                    <div class="modal-body">
                      <div class="row form-group">
                           <div class="col col-md-3">
                             <label for="who_are_we" class=" form-control-label">Who Are We description (2)</label>
                           </div>
                           <div class="col-12 col-md-9">
                             <textarea name="who_are_we"  rows="9"  class="form-control ckeditor">{{$about->who_are_we}}</textarea>
                           </div>
                         </div>
                         <div class="row form-group">
                           <div class="col col-md-3">
                             <label for="who_are_we_image1" class=" form-control-label">Who Are We First Image (3)</label><br>
                             <span class="au-breadcrumb-span">[ Note: Please upload the image size 470*282 and should be less than 100kb ]</span>
                           </div>
                           <div class="col-12 col-md-9 process">
                             <input type="file" id="who_are_we_image1" accept="image/png, image/jpg, image/jpeg" name="who_are_we_image1" class="form-control-file">
                             <img src="/uploads/about-us/{{$about->who_are_we_image1}}" alt="image">
                           </div>
                         </div>
                         <div class="row form-group">
                          <div class="col col-md-3">
                             <label for="who_are_we_image2" class=" form-control-label"> Who Are We First Image (4)</label><br>
                             <span class="au-breadcrumb-span">[ Note: Please upload the image size 470*282 and should be less than 100kb ]</span>
                           </div>
                           <div class="col-12 col-md-9 process">
                             <input type="file" id="who_are_we_image2" accept="image/png, image/jpg, image/jpeg" name="who_are_we_image2" class="form-control-file">
                             <img src="/uploads/about-us/{{$about->who_are_we_image2}}" alt="Image">
                           </div>
                         </div>
                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

                </div>
              </form>
            </div>
         </div>
         <script>
          function editaboutus(){
            $('#aboutuschange').modal('show');
          }
          </script>
  @endif
</section>
 <section class=""style="position: relative;">
           <video autoplay="" loop="" muted="" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 0%; transform: translate(0%, -50%); visibility: visible; opacity: 1; width: 1888px; height: auto;" __idm_id__="683964417">
           <source src="/uploads/about-us/{{$about->video}}" type="video/mp4">
           </video>
        <div class="section-sm context-dark text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-10">
                <div class="pb-5">
                  <?php echo ($about->video_title)?>
                </div>
              </div>
            </div>
          </div>
        </div>
        @if(Auth::check())
          <div class="admin-visibility">
            <i class="video fa fa-pencil" onclick="editvideochange()">
            </i>
          </div>
          <div class="modal fade" id="videopart" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form action="/home/videochange" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                      </div>

                    <div class="modal-body">
                         <div class="row form-group">
                           <div class="col col-md-3">
                             <label for="video_title" class=" form-control-label">Video Title (5)</label>
                           </div>
                           <div class="col-12 col-md-9">
                             <textarea name="video_title"  rows="9"  class="form-control ckeditor">{{$about->video_title}}</textarea>
                           </div>
                         </div>
                         <div class="row form-group">
                           <div class="col col-md-3">
                             <label for="video" class=" form-control-label">Video (7)</label><br>
                             <span class="au-breadcrumb-span">[ Note: Please upload the video less than 5-10mb ]</span>
                           </div>
                           <div class="col-12 col-md-9">
                             <input type="file" name="video" class="form-control-file file_multi_video" accept="video/mp4,video/avi,video/mpeg,video/MKV">

                             <video style="margin-top: 20px;" width="350" controls>
                               <source src="/uploads/about-us/{{$about->video}}" id="video_here">
                                 Your browser does not support HTML5 video.
                               </video>
                             </div>
                           </div>
                      </div>
                      <div class="modal-footer">
                         <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                </div>
              </form>
            </div>
          </div>

          <script>
          function editvideochange(){
            $('#videopart').modal('show');
          }
          </script>
          @endif
      </section>

<section id="haruyosi_about" class="section section-md bg-default text-center" style="position: relative">
  <div class="container">
    <h2>Our Professional Team</h2>
    <div class="divider-lg"></div>
    <p class="block-lg">{{$about->team_description}} </p>
    <div class="row row-30">
      <div class="col-12">
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-creative" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($staffs as $staff)
          <div class="team-minimal team-minimal-with-shadow">
            <figure><a href="" data-toggle="modal" data-target="#myModal{{$staff->id}}"><img src="/uploads/{{$staff->image}}" alt="" width="370" height="370"></a></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title"><a href="#" data-toggle="modal" data-target="#myModal{{$staff->id}}">{{$staff->name}} </a></h4>
              <p>{{$staff->designation}}</p>
            </div>
          </div>
          @endforeach
        </div>

        @foreach($staffs as $staff)
        <!-- Modal -->
        <div class="modal fade" id="myModal{{$staff->id}}" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <section class="section bg-default">
                  <div class="container">
                    <div class="row row-50">
                      <div class="col-lg-6 col-xl-4 text-center">
                        <div class="row row-50">
                          <div class="col-12"><img class="img-shadow" src="/uploads/{{$staff->image}}" alt="" width="370" height="370"/>
                          </div>
                          <div style="margin-bottom: 20px;" class="col-12"><a style="padding: 8px 20px;" class="button button-primary" href="mailto:{{$staff->mail}}">Mail Me</a></div>
                          <!--                           <div class="col-12"><a style="padding: 10px 15px;" class="button button-primary" href="/contacts">Make an appointment</a></div> -->
                          <div class="col-12">
                            <ul class="list-inline social-list">
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-facebook" target="_blank" href="{{$staff->facebook_link}}"></a></li>
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-twitter" target="_blank" href="{{$staff->twitter_link}}"></a></li>
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-instagram" target="_blank" href="{{$staff->instagram_link}}"></a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6 col-xl-8">
                        <div class="box-team-info">
                          <div class="box-team-info-header">
                            <h3>{{$staff->name}}</h3>
                            <p>{{$staff->designation}}</p>
                            <div class="divider-lg"></div>
                          </div>
                          <div style="text-align: justify;"><?php echo ($staff->description)?></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
              <div style="padding: 30px;" class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <!-- <div class="col-12"><a class="button button-default-outline" href="/our-team">View all team</a></div> -->
    </div>
  </div>
  @if(Auth::check())
          <div class="admin-visibility">
            <i class="teamdesc fa fa-pencil" onclick="editteamdisc()">
            </i>
          </div>
          <div class="modal fade" id="teamdescription" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form action="/home/teamdescript" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div class="row form-group">
                   <div class="col col-md-3">
                    <label for="team_description" class=" form-control-label">Team Description (8)</label>
                  </div>
                 <div class="col-12 col-md-9">
                   <textarea name="team_description"  rows="9"  class="form-control ckeditor">{{$about->team_description}}</textarea>
                   <!-- <input style="height: 60px;" type="text" name="team_description"  rows="9"  class="form-control ckeditor" value="{{$about->team_description}}" placeholder="Video url"><br> -->
                </div>
                 </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>

              </div>
            </form>
            </div>
          </div>
          <script>
          function editteamdisc(){
            $('#teamdescription').modal('show');
          }
          </script>

          @endif
</section>

<section class="section-transform-bottom">
  <div class="container-fluid section-md bg-primary context-dark">
    <div style="margin-right: 0px;" class="row justify-content-center row-50">
      <div class="col-sm-10 text-center">
        <h2>Subscribe to Our Newsletter</h2>
        <div class="divider-lg"></div>
      </div>
      <div class="col-sm-10 col-lg-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}<br></li>
            @endforeach
          </ul>
        </div>
        @endif
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif
        <!-- RD Mailform-->
        <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
          @csrf
          <div class="form-wrap">
            <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
            <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
          </div>
          <div class="form-button1">
            <button class="button button-primary" type="submit">Subscribe</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

@endsection