 @extends('layouts.new.app', ['title' => 'Portfolio'],['discription'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title" style="background-image: url(images/banner/{{$portfolioBanner->portfolio_banner}}); background-size: cover;position: relative;">
  <div class="container">
    <h1 class="page-title">Portfolio</h1>
  </div>
  @if(Auth::check())
  <div class="admin-visibility">
   <i class="portfoliobanner fa fa-pencil" onclick="portfoliobanner()">
   </i>
 </div>
 <div class="modal fade" id="portfoliochange" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
     <form action="/home/portfoliobanner" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="row form-group">
       <div class="col col-md-3">             
         <label for="banner_image" class=" form-control-label">Banner Image input (1)</label><br>
         <span class="au-breadcrumb-span">[ Note: Please upload the image size [1920*305] & should be less than 100kb ]</span>
       </div>
       <div class="col-12 col-md-9 process">
        <input type="file" onchange="readURL(this);" class="form-control" name="portfolio_banner"  accept="image/png, image/jpg, image/jpeg">
        <br><img id="blah" src="/images/banner/{{$portfolioBanner->portfolio_banner}}" alt="Selected Image" />
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
</div>
</div>
</div>
<script>
function portfoliobanner(){
  $('#portfoliochange').modal('show');
}
</script>
@endif
</section>

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">Portfolio</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default text-center" style="position: relative">
  <div class="container">
    <h2>All Portfolio</h2>
    <div class="divider-lg"></div>
    <?php echo ($homepages->portfolio_content)?>
         @if(Auth::check())
          <div class="admin-visibility">
            <i class="portfoliochange fa fa-pencil" onclick="portfolioclick()">
            </i>
          </div>
          <div class="modal fade" id="portfolioupdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form action="/home/portfolio" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                 <div class="modal-body">
                  <div class="row form-group">
                    <div class="col col-md-3">
                      <label for="portfolio_content" class=" form-control-label">Portfolio Description (18)</label>
                    </div>
                     <div class="col-12 col-md-9">
                        <textarea name="portfolio_content"  rows="9"  class="form-control ckeditor">{{$homepages->portfolio_content}}</textarea>
                    </div>
                  </div>
                </div>

                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </div>
            </form>
          </div>
        </div>
         <script>
          function portfolioclick(){
            $('#portfolioupdate').modal('show');
          }
          </script>
          @endif
    <div class="row row-30">
      <div class="col-lg-12">
        <div class="isotope-filters isotope-filters-horizontal">
          <button class="isotope-filters-toggle button button-sm button-primary" data-custom-toggle="#isotope-filters" data-custom-toggle-disable-on-blur="true">Filter<span class="caret"></span></button>
          <ul class="isotope-filters-list" id="isotope-filters">
            <li><a class="active" data-isotope-filter="*" data-isotope-group="gallery" href="#">All</a></li>
            @foreach($gallerycategories as $gallerycat)
            <li><a data-isotope-filter="{{$gallerycat->id}}" data-isotope-group="gallery" href="#">{{$gallerycat->title}} </a></li>
            @endforeach
          </ul>
        </div>
      </div>
      </div>
    </div>
      <!-- Isotope Content-->
      <div class="container-fluid container-responsive">
      <div class="col-lg-12">
        <div class="isotope row" data-isotope-layout="masonry" data-isotope-group="gallery" data-lightgallery="group" data-column-class=".col-sm-6.col-lg-4">
          @foreach($gallerycategories as $gallcat)
          @foreach ($gallery as $galle)
          @if($gallcat->id == $galle->cat_id)
          <div class="col-sm-6 col-lg-4 isotope-item" data-filter="{{$galle->cat_id}}"><a class="gallery-item"  href="{{$galle->url}}"><img class="port-img" src="/uploads/gallery/{{$galle->file}}" alt="" width="570" height="570"/><span class="gallery-item-title">
            <div class="form-button1"><i class="fa fa-external-link" aria-hidden="true"></i><!-- <button class="button button-primary" type="submit">Demo</button> --></div></span><!-- <span class="gallery-item-button"></span> --></a>
            <span style="text-transform: capitalize;" class="gallery-item-title">{{$galle->name}}</span>
            
          </div>
          @endif
          @endforeach
          @endforeach
        </div>
      </div>
      </div>
      <div class="col-12">
        <ul class="pagination">
          <li style="margin: 0 auto;" class="page-item"></li>
        </ul>
      
  </div>
</section>
        <section class="section-transform-bottom">
          <div class="container-fluid section-md bg-primary context-dark">
            <div style="margin-right: 0px;" class="row justify-content-center row-50">
              <div class="col-sm-10 text-center">
                <h2>Subscribe to Our Newsletter</h2>
                <div class="divider-lg"></div>
              </div>
              <div class="col-sm-10 col-lg-6">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}<br></li>
                    @endforeach
                  </ul>
                </div>
                @endif
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
                @endif
                <!-- RD Mailform-->
                <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
                  @csrf
                  <div class="form-wrap">
                    <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
                    <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
                  </div>
                  <div class="form-button1">
                    <button class="button button-primary" type="submit">Subscribe</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
<!-- Page Footer-->
@endsection