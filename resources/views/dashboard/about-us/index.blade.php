@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Dashboard</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/about-us">AboutUs Contents</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12">
				<a style="margin-bottom: 10px; float: right;" href="{{route('about-us.edit', 1)}}"><button class="btn btn-primary make-btn">Edit AboutUs Contents</button></a>
				<!-- <a style="margin-bottom: 10px;" href="/backoffice/about-us/create" class="btn btn-primary">Add New Service</a> -->
				<!-- DATA TABLE-->
				<div class="table-responsive m-b-40">
					<table class="table table-borderless table-data3">
						<thead>
							<tr>
								<th>Banner Image</th>
								<th>Who Are We Text</th>
								<th>Who Are We First Image</th>
								<th>Who Are We Second Image</th>
								<th>Video Title</th>
								<th>Video Image</th>
								<th>Video</th>
								<th>Team Description</th>
							</tr>
						</thead>
						<tbody>
							@foreach($aboutus as $about)
							<tr>
								<td class="process"><img src="/uploads/about-us/{{$about->banner_image}}"></td>
								<td><?php echo ($about->who_are_we)?></td>
								<td class="process"><img src="/uploads/about-us/{{$about->who_are_we_image1}}"></td>
								<td class="process"><img src="/uploads/about-us/{{$about->who_are_we_image2}}"></td>
								<td><?php echo ($about->video_title)?></td>
								<td class="process"><img src="/uploads/about-us/{{$about->video_image}}"></td>
								<td><iframe src="{{$about->video}}" volume="0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</td>
								<td>{{$about->team_description}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
			<a style="margin: 10px;" href="{{route('about-us.edit', 1)}}"><button class="btn btn-primary make-btn">Edit AboutUs Contents</button></a>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection