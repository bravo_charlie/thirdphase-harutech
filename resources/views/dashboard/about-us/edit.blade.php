@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Dashboard</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">About Us Content</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<span class="au-breadcrumb-span note-space">[Note: <a href="#help_image">Click For Help</a>]</span>
			<form action="/backoffice/about-us/edit/{{$aboutus->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<button style="float: right;" type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="banner_image" class=" form-control-label">Banner Image input (1)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size [1920*305] & should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" id="banner_image" accept="image/png, image/jpg, image/jpeg" name="banner_image" class="form-control-file">
						<img src="/uploads/about-us/{{$aboutus->banner_image}}" alt="{{$aboutus->name}}">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="who_are_we" class=" form-control-label">Who Are We description (2)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="who_are_we"  rows="9"  class="form-control ckeditor">{{$aboutus->who_are_we}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="who_are_we_image1" class=" form-control-label">Who Are We First Image (3)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 470*282 and should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" id="who_are_we_image1" accept="image/png, image/jpg, image/jpeg" name="who_are_we_image1" class="form-control-file">
						<img src="/uploads/about-us/{{$aboutus->who_are_we_image1}}" alt="image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="who_are_we_image2" class=" form-control-label"> Who Are We First Image (4)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 470*282 and should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" id="who_are_we_image2" accept="image/png, image/jpg, image/jpeg" name="who_are_we_image2" class="form-control-file">
						<img src="/uploads/about-us/{{$aboutus->who_are_we_image2}}" alt="Image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="video_title" class=" form-control-label">Video Title (5)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="video_title"  rows="9"  class="form-control ckeditor">{{$aboutus->video_title}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="video" class=" form-control-label">Video (7)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the video less than 5-10mb ]</span>
					</div>
					<div class="col-12 col-md-9">
						<input type="file" name="video" class="form-control-file file_multi_video" accept="video/mp4,video/avi,video/mpeg,video/MKV">

						<video style="margin-top: 20px;" width="400" controls>
							<source src="/uploads/about-us/{{$aboutus->video}}" id="video_here">
								Your browser does not support HTML5 video.
							</video>
						</div>
					</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="team_description" class=" form-control-label">Team Description (8)</label>
					</div>
					<div class="col-12 col-md-9">
						<input style="height: 60px;" type="text" name="team_description"  rows="9"  class="form-control" value="{{$aboutus->team_description}}" placeholder="Video url"><br>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
			</form>
		</div>
		<div id="help_image" class="card-footer">
			<span class="au-breadcrumb-span">[The below Numbers in the image denotes upper number for changes.]  </span><br><br
			<div class="row">
				<!-- <div class="col-md-6"> -->
					<div class="col-md-12 help-image">
						<img src="/images/about-us-image.jpg" alt="Image">
					</div>
				</div>
			</div>
		</div>
	</div>



</div><!--/.col-->

@endsection