@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->

<div class="copyright">
  <h2>All The Newsletter Posts</h2>
  @if (Session::has('success'))
  <div class="alert alert-success text-center">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <p>{{ Session::get('success') }}</p>
  </div>
  @endif
</div>
<span style="margin: 0 0 20px 10px; font-size: 18px " class="au-breadcrumb-span">[ Note: To add new post in the Newsletter go to the link below and select the post category : <b>newsletter</b> ]</span><br>
<a style="margin: 0 0 10px 10px;" href="/backoffice/news/create" class="btn btn-primary">Add New Newsletter Post</a>
<table class="m_5772815688879916528tableWide" width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#e6e7e8">
  <tbody>
    <tr>
      <td class="m_5772815688879916528table" style="border-collapse:collapse;padding:20px 0px;" align="center" >SN.</td>
      <td class="m_5772815688879916528table" style="border-collapse:collapse;padding:20px 0px;" align="center" >Newsletter Posts</td>
      <td class="m_5772815688879916528table" style="border-collapse:collapse;padding:20px 30px 20px 0;" align="center" >Action</td>
    </tr>
    @if(count($blogs))
    @foreach($blogs as $news)
    @if($news->cat_id ==3)
    <tr>
      <td class="m_5772815688879916528table" style="border-collapse:collapse;padding-bottom:50px;padding-left:50px;padding-top:10px" align="center" >{{ $loop->iteration }}</td>
      <td class="m_5772815688879916528tableWhiteBackground" style="border-collapse:collapse" width="100%" bgcolor="#e6e7e8" valign="top" align="center">
        <table class="m_5772815688879916528table" width="600" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td class="m_5772815688879916528table" style="border-collapse:collapse;padding:30px 0" align="center" valign="top" bgcolor="#2a81f4">
                <table class="m_5772815688879916528table90" width="450" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td class="m_5772815688879916528table" style="border-collapse:collapse" width="450" height="3" align="center" valign="top" bgcolor="#2a81f4">
                        <a href="/news-detail/{{$news->id}}" style="line-height:39px;text-transform: capitalize; text-decoration:none;color:#ffffff;font-size:35px;font-weight:bold;font-family:helvetica,arial,sans-serif">{{$news->title}}</a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table class="m_5772815688879916528hide" width="600" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td style="border-collapse:collapse;padding:30px 0;padding-bottom:10px" height="2" align="center" width="600" bgcolor="#fff" valign="bottom">
                <a href="/news-detail/{{$news->id}}"><img class="m_5772815688879916528table CToWUd" style="color:#ffffff;outline:none;text-decoration:none;display:block" src="/uploads/{{$news-> f_image}}" width="185" border="0" alt="{{$news->title}}">
                </a>
              </td>
            </tr>
          </tbody>
        </table>

        <div style="display:none;width:0px;max-height:0px;overflow:hidden" class="m_5772815688879916528tableVisibleFeature">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td style="border-collapse:collapse" width="100%" bgcolor="#2a81f4" align="center" valign="top">
                  <a href="/news-detail/{{$news->id}}"><img style="color:#ffffff;outline:none;text-decoration:none;display:block;padding-top:40px" src="uploads/{{$news-> f_image}}" width="185" border="0" alt="{{$news->title}}">
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <table class="m_5772815688879916528table" width="600" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td class="m_5772815688879916528table" style="border-collapse:collapse;padding:40px 0" align="center" valign="top" bgcolor="#2a81f4">
                <table class="m_5772815688879916528table90" width="80%" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td class="m_5772815688879916528table" style="border-collapse:collapse" width="600" height="3" align="center" valign="top" bgcolor="#2a81f4">
                        <font style="text-decoration:none;color:#ffffff;font-size:16px;font-weight:400;line-height:20px;font-family:helvetica,arial,sans-serif">
                          <p><?php 
                          $excerpt = $news->description;
                          $the_str = substr($excerpt, 0, 130);
                          echo $the_str; 
                          ?>...</p>
                          <a href="/news-detail/{{$news->id}}" class="btn btn-primary" style="text-decoration:none;margin-top:10px;color:#ffdf88;font-size:12px;font-weight:bold;font-family:helvetica,arial,sans-serif" target="_blank">READ MORE ⟩</a>
                        </font>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="m_5772815688879916528table" style="border-collapse:collapse;padding-bottom:50px;padding-top:10px" align="center" >
        <form action="/backoffice/newsletter" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="description" value="<?php $excerpt = $news->description;$the_str = substr($excerpt, 0, 130);echo $the_str;?>...">
          <input type="hidden" name="title" value="{{$news->title}}">
          <input type="hidden" name="image" value="{{$news-> f_image}}">
          <input type="hidden" name="id" value="{{$news-> id}}">
          <button style="margin-right: 30px;" onclick="makeConformation(event)" class="btn btn-primary">Share</button>
        </form> <br>
        <form action="{{route('delete-post.blog',$news->id)}}">
          <button type="submit" style="margin-right: 30px;" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
        </form>
      </td>
    </tr>
    <tr>
      <td class="m_5772815688879916528table" style="border-collapse:collapse;padding-bottom:50px;padding-top:10px" align="center" ></td>
    </tr>
    @endif
    @endforeach
    @else
    <h5>There are no posts in Newsletter.</h5>
    @endif
  </tbody>
</table>
<script type="text/javascript">
  function makeWarning(evt){
    let result = confirm("Are you sure to Delete?");
    if(! result){
      evt.stopPropagation();
      evt.preventDefault();   
    }
  }
</script>
<script type="text/javascript">
  function makeConformation(evt){
    let result = confirm("Do You want to Share This Post!");
    if(! result){
      evt.stopPropagation();
      evt.preventDefault(); 
    }
  }
</script>
@endsection