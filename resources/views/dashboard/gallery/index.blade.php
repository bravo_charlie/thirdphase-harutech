@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/gallery">Portfolio</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12 dashboard-space">
				<h4>Banner Image</h4><br>
				<div class="job-banner-image">
					<a href="#" data-toggle="modal" data-target="#myBanner">
						<img src="/images/banner/{{$galleryBanner->portfolio_banner}}">
						<span style="margin: 20px 0 0 20px;" class="btn btn-primary">Edit Banner Image</span></a>
					</div>
					<!-- Modal -->
					<div class="modal fade" id="myBanner" role="dialog">
						<div class="modal-dialog">
							
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Portfolio Banner Image</h4>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<form method="post" action="/backoffice/banner/image/{{$galleryBanner->id}}" enctype="multipart/form-data"> 

										@csrf
										<div class="field">
											<!-- image input-->
											<div class="form-group">
												<label class="control-label" for="portfolio_banner">Select Image:</label>
												<div class="">
													<input type="file" onchange="readURL(this);" class="form-control" name="portfolio_banner"  accept="image/png, image/jpg, image/jpeg">
													<br><img id="blah" src="/images/banner/{{$galleryBanner->portfolio_banner}}" alt="Selected Image" />
												</div>
											</div>
											<button style="float: left;" type="submit" class="btn btn-primary">submit</button></div>
											<a href="#" style="float: left;margin-left: 10px;" class="btn btn-danger" data-dismiss="modal">Close</a>
										</form>
									</div>
									<div class="modal-footer">
									</div>
								</div>
								
							</div>
						</div>
						<br>
						<a style="margin: 0 0 20px 20px ;" href="/backoffice/gallery/create" class="btn btn-primary">Add New Image</a>
						<!-- <a style="margin-bottom: 10px;" href="/backoffice/gallery/createVideo" class="btn btn-primary">Add New Video</a> -->
						<!-- DATA TABLE-->
						<div class="table-responsive m-b-40">
							<table class="table table-borderless table-data3">
								<thead>
									<tr>
										<th>Id</th>
										<th>Name</th>
										<th>Category</th>
										<th>Url</th>
										<th>Image</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($gallery as $galler)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$galler->name}}</td>
										<td>
											@foreach($gallerycategories as $Postcat)
											@if($Postcat->id == $galler->cat_id)
											{{$Postcat->title}}
											@endif
											@endforeach
										</td>
										<td>{{$galler->url}}</td>
										<?php 
										$string = $galler->file;
										$string = substr(strrchr($string, '.'), 1);
										?>
										@if($string == "jpg" || $string == "png" || $string == "jpeg")
										<td class="process"><img src="/uploads/gallery/{{$galler->file}}"></td>
										@else
										<td class="process"><img src="/uploads/gallery/{{$galler->file}}"></td>
										@endif
										<td class="make_btn_straight"><a href="{{route('gallery.edit', $galler->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
											<form method="post" action="{{route('gallery.delete',$galler->id)}}">
												@csrf
												{{ method_field('DELETE') }}
												<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="col-12">
							<ul class="pagination">
								<li style="margin: 0 auto;" class="page-item">{{ $gallery->links() }}</li>
							</ul>
						</div>
						<!-- END DATA TABLE-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
	</script>

	@endsection