 @extends('layouts.new.app', ['title' => 'Contact'],['discription'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title"
  style="background-image: url(images/banner/{{$contact->contact_banner}});
   background-size: cover; position: relative;">
  <div class="container">
    <h1 class="page-title">Contacts</h1>
  </div>
  @if(Auth::check())
  <div class="admin-visibility">
     <i class="servicebanner fa fa-pencil" onclick="contactbanner()">
     </i>
  </div>
  <div class="modal fade" id="contactchange" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
     <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="/home/contactbanner" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row form-group">
         <div class="col col-md-3">             
           <label for="banner_image" class=" form-control-label">Banner Image input (1)</label><br>
           <span class="au-breadcrumb-span">[ Note: Please upload the image size [1920*305] & should be less than 100kb ]</span>
         </div>
         <div class="col-12 col-md-9 process">
          <input onchange="readURL(this);" type="file" id="contact_banner" accept="image/png, image/jpg, image/jpeg" name="contact_banner" class="form-control-file"><br>
          <img id="blah" src="/images/banner/{{$contact->contact_banner}}" alt="{{$contact->name}}">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
</div>
</div>
<script>
function contactbanner(){
  $('#contactchange').modal('show');
}
</script>
          @endif
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">Contacts</li>
    </ul>
  </div>
</section>
<!-- Mailform-->
<section class="section section-md" style="position: relative;">
  <div class="container container-responsive">
    <div class="row row-50">
      <div class="col-lg-8">
        <h2>Contact us</h2>
        <div class="divider-lg"></div>
        <?php echo ($contact->banner_text)?>
        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}<br></li>
              @endforeach
            </ul>
          </div>s
          @endif
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif
      <!-- RD Mailform-->
      <form class="text-left" action="{{url('sendemail/send')}}" method="post" >
        @csrf
        <div class="row row-15">
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-name" type="text" name="name" placeholder="First name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-sec-name" type="text" name="sec_name" placeholder="Last name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-phone" type="text" placeholder="Phone" name="phone" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-email" type="email" name="email" placeholder="E-Mail" required="">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-wrap">
              <input class="form-input" id="contact-subject" type="subject" placeholder="Subject" name="subject" required="">
            </div>
          </div>
          <div class="col-12">
            <div class="form-wrap">
              <!-- <label class="form-label" for="contact-message"></label> -->
              <textarea class="form-input" id="contact-message" placeholder="Message" name="message" required=""></textarea>
            </div>
          </div>
        </div>
        <div class="form-button group-sm text-left">
          <button class="button button-primary" type="submit">Send message</button>
        </div>
      </form>
    </div>
    <div class="col-lg-4">
      <ul class="contact-list">
        <li> 
          <p class="contact-list-title">Address</p>
          <div class="contact-list-content"><span class="icon mdi mdi-map-marker icon-primary"></span>
            <?php echo ($contact->address)?>
          </div>
        </li>
        <li>
          <p class="contact-list-title">Phone</p>
          <div class="contact-list-content"><span class="icon mdi mdi-phone icon-primary"></span><a href="tel:{{$contact->phone}}">{{$contact->phone}}</a></div>
        </li>
        <li>
          <p class="contact-list-title">E-mail</p>
          <div class="contact-list-content"><span class="icon mdi mdi-email-outline icon-primary"></span><a href="mailto:{{$contact->mail}}"><span>{{$contact->mail}}</span></a></div>
        </li>
        <li>
          <p class="contact-list-title">Opening Hours</p>
          <div class="contact-list-content"><span class="icon mdi mdi-clock icon-primary"></span>
            <?php echo ($contact->opening_hour)?>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
@if(Auth::check())
          <div class="admin-visibility">
            <i class="conatctdetail fa fa-pencil" onclick="contactinfo()">
            </i>
          </div>
          <div class="modal fade" id="contactlocation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form action="/home/contactchangetext" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div class="row form-group">
                       <div class="col col-md-3">
                         <label for="address" class=" form-control-label">Address (3)</label>
                      </div>
                       <div class="col-12 col-md-9">
                         <input type="text" name="address"  rows="9"  class="form-control" value="{{$contact->address}}" placeholder="Address"><br>
                       </div>
                     </div>
                    <div class="row form-group">
                       <div class="col col-md-3">
                         <label for="address" class=" form-control-label">Phone Number (4)</label>
                       </div>
                       <div class="col-12 col-md-9">
                        <input type="text" name="phone"  rows="9"  class="form-control" value="{{$contact->phone}}" placeholder="phone"><br>
                       </div>
                     </div>
                    <div class="row form-group">
                       <div class="col col-md-3">
                        <label for="address" class=" form-control-label">Mail Address (5)</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" name="mail"  rows="9"  class="form-control" value="{{$contact->mail}}" placeholder="mail"><br>
                       </div>
                    </div>
                     <div class="row form-group">
                      <div class="col col-md-3">
                         <label for="opening_hour" class=" form-control-label">Opening Hours (7)</label>
                      </div>
                      <div class="col-12 col-md-9">
                         <textarea name="opening_hour"  rows="9"  class="form-control ckeditor">{{$contact->opening_hour}}</textarea>
                      </div>
                     </div>
                </div>

                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

              </div>
              </form>
            </div>
          </div>
          <script>
          function contactinfo(){
            $('#contactlocation').modal('show');
          }
          </script>
@endif
</section>
<!-- Page Footer-->
<!-- Google map-->
<section class="section" style="position: relative;">
  <div class="google-map-container">
  <?php echo ($contact->map)?>
  </div>
   @if(Auth::check())
          <div class="admin-visibility">
            <i class="about fa fa-pencil" onclick="mapchange()">
            </i>
          </div>
          <div class="modal fade" id="mapheader" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form action="/home/mapiframe" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                   <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Edit the content</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
               <div class="modal-body">
               <div class="row form-group">
                <div class="col col-md-3">
                 <label for="address" class=" form-control-label">Map Iframe (6)</label>
               </div>
               <div class="col-12 col-md-9">
                <textarea name="map"  rows="9" placeholder="map Iframe" class="form-control ckeditor">{{$contact->map}}</textarea>
               <!-- <input type="text"   rows="9"  class="form-control" value="{{$contact->map}}" placeholder="map Iframe"> --><br>
              </div>
             </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </div>
              </form>
            </div>
          </div>
          <script>
          function mapchange(){
            $('#mapheader').modal('show');
          }
          </script>
            @endif
</section>
@endsection