<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\about_us_page;

class aboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $about = new about_us_page();
        $about->banner_image = 'default-thumbnail.png';
        $about->who_are_we = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $about->who_are_we_image1 = 'default-thumbnail.png';
        $about->who_are_we_image2 = 'default-thumbnail.png';
        $about->video_title = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $about->video_image = 'default-thumbnail.png';
        $about->video = 'https://www.youtube.com/embed/AuVHftBiDVw?autoplay=1&mute=1';
        $about->team_description = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $about->save();
    }
}