<?php

namespace App\Http\Controllers;

use App\Model\Page;
use Illuminate\Http\Request;
use App\PageSetting;
use App\homePage;
use App\about_us_page;
use App\job;
use App\Jobs_detail;
use App\Blog;
use App\Blogcategory;
use App\Service;
use App\Testimonial;
use App\ContactDetails;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('backend.pages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }

    public function topheader(Request $request){
        $page = homepage::findOrFail('1');
        $page->update($request->all());
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }

    public function logochange(Request $request){
        $pages = PageSetting::findOrFail('1');
        if(file_exists($request->file('site_logo'))){
            $image = $request->file('site_logo');
            $imageName =  "home".time().'.'.$request->file('site_logo')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $pages->site_logo = $imageName;
        }
        else{
            $pages->site_logo = $pages->site_logo;
        }
        $pages->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function whyussection(Request $request){
        $page = homepage::findOrFail('1');
        $page->upper_body_content = $request->upper_body_content;
	 
        if(file_exists($request->file('upper_body_image1'))){
            $image = $request->file('upper_body_image1');
            $iimageName =  "home".time().'.'.$request->file('upper_body_image1')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$iimageName);
            $page->upper_body_image1 = $iimageName;
        }
        else{
            $page->upper_body_image1 = $page->upper_body_image1;
        }
        if(file_exists($request->file('upper_body_image2'))){
            $image = $request->file('upper_body_image2');
            $imageName =  "home".time().'.'.$request->file('upper_body_image2')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $page->upper_body_image2 = $imageName;
        }
        else{
            $page->upper_body_image2 = $page->upper_body_image2;
        }
        $page->save();
       return redirect()->back()->with('success', 'Updated Successfully!!!');
    }

    public function homeService(Request $request){
        $page = homepage::findOrFail(1);
        $page->service_body_content = $request->service_body_content;
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }

    public function interior(Request $request){
        $page = homepage::findOrFail(1);
        $page->lower_body_content = $request->lower_body_content;
        if(file_exists($request->file('lower_body_image1'))){
            $file = "homepages".time().'.'.$request->file('lower_body_image1')->getclientOriginalExtension();
            $location = public_path('uploads/homepage');
            $request->file('lower_body_image1')->move($location, $file);
            $page->lower_body_image1 = $file;
        }
        else{
            $page->lower_body_image1 =  $page->lower_body_image1;
        }

        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function portfolio(Request $request){
        $page = homepage::findOrFail(1);
        $page->portfolio_content = $request->portfolio_content;
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function aboutchange(Request $request){
        $page = about_us_page::findOrFail(1);
        $page->who_are_we = $request->who_are_we;
        if(file_exists($request->file('who_are_we_image1'))){
            $image = $request->file('who_are_we_image1');
            $imageName =  "who_are_we_image1".time().'.'.$request->file('who_are_we_image1')->getClientOriginalName();
            $image->move(public_path('uploads/about-us'),$imageName);
            $page->who_are_we_image1 = $imageName;
        }
        else{
            $page->who_are_we_image1 = $page->who_are_we_image1;
        }
        if(file_exists($request->file('who_are_we_image2'))){
            $image = $request->file('who_are_we_image2');
            $imageName =  "who_are_we_image2".time().'.'.$request->file('who_are_we_image2')->getClientOriginalName();
            $image->move(public_path('uploads/about-us'),$imageName);
            $page->who_are_we_image2 = $imageName;
        }
        else{
            $page->who_are_we_image2 = $page->who_are_we_image2;
        }
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function aboutbannerchange(Request $request){
        $page = about_us_page::findOrFail(1);
        if(file_exists($request->file('banner_image'))){
            $image = $request->file('banner_image');
            $imageName =  "banner_image".time().'.'.$request->file('banner_image')->getClientOriginalName();
            $image->move(public_path('uploads/about-us'),$imageName);
            $page->banner_image = $imageName;
        }
        else{
            $page->banner_image = $page->banner_image;
        }
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function aboutvideochange(Request $request){
        $request->validate([
            'video' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/mkv,qt | max:500000 ',
        ]);

        $page = about_us_page::findOrFail(1);
        $page->video_image = $request->video_image;
        $page->video_title = $request->video_title;
        if(file_exists($request->file('video'))){
            $file = "about".time().'.'.$request->file('video')->getclientOriginalExtension();
            $location = public_path('uploads/about-us');
            $request->file('video')->move($location, $file);
            $page->video = $file;
        }
        else{
            $page->video =  $page->video;
        }
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function teamdescription(Request $request){
       $page = about_us_page::findOrFail(1);
       $page->team_description = $request->team_description;
       $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function servicewith(Request $request){
        $page = homepage::findOrFail('1');
        $page->upper_body_content = $request->upper_body_content;
     
        if(file_exists($request->file('upper_body_image1'))){
            $image = $request->file('upper_body_image1');
            $iimageName =  "home".time().'.'.$request->file('upper_body_image1')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$iimageName);
            $page->upper_body_image1 = $iimageName;
        }
        else{
            $page->upper_body_image1 = $page->upper_body_image1;
        }
        if(file_exists($request->file('upper_body_image2'))){
            $image = $request->file('upper_body_image2');
            $imageName =  "home".time().'.'.$request->file('upper_body_image2')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $page->upper_body_image2 = $imageName;
        }
        else{
            $page->upper_body_image2 = $page->upper_body_image2;
        }
        $page->save();
       return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function servicebannerchange(Request $request){
        $page = Jobs_detail::findOrFail(1);
        if(file_exists($request->file('service_banner'))){
            $image = "banner".time().'.'.$request->file('service_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('service_banner')->move($location, $image);
            $page->service_banner = $image;
        }
        else{
            $page->service_banner = $page->service_banner ;
        }
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function contactchange(Request $request){
        $page = ContactDetails::findOrFail(1);
        if(file_exists($request->file('contact_banner'))){
            $image = "banner".time().'.'.$request->file('contact_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('contact_banner')->move($location, $image);
            $page->contact_banner = $image;
        }
        else{
            $page->contact_banner = $page->contact_banner;
        }        
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function contactchangepart(Request $request){
        $page = ContactDetails::findOrFail(1);
         $page->address = $request->address;
        $page->phone = $request->phone;
        $page->mail = $request->mail;
        $page->opening_hour = $request->opening_hour; 
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function contactiframe(Request $request){
         $page = ContactDetails::findOrFail(1);
         $page->map = $request->map;
         $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function portfoliobannerchange(Request $request){
        $page = Jobs_detail::findOrFail('1');
        if(file_exists($request->file('portfolio_banner'))){
            $image = "banner".time().'.'.$request->file('portfolio_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('portfolio_banner')->move($location, $image);
            $page->portfolio_banner = $image;
        }
        else{
            $page->portfolio_banner = $page->portfolio_banner ;
        }
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function newsbannerchange(Request $request){
        $page = Jobs_detail::findOrFail(1);
        if(file_exists($request->file('blog_banner'))){
            $image = "banner".time().'.'.$request->file('blog_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('blog_banner')->move($location, $image);
            $page->blog_banner = $image;
        }
        else{
            $page->blog_banner = $page->blog_banner ;
        }
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function scheduleservicechange(Request $request){
        $page = homepage::findOrFail(1);
        $page->schedule_content = $request->schedule_content;
        $page->why_us_content = $request->why_us_content;
        if(file_exists($request->file('contact_body_content'))){
            $image = $request->file('contact_body_content');
            $imageName =  "why_schedule_image".time().'.'.$request->file('contact_body_content')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $page->contact_body_content = $imageName;
        }
        else{
            $page->contact_body_content = $page->contact_body_content;
        }
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function servciechagedesc(Request $request){
        $page = homepage::findOrFail(1);
        $page->service_body_content = $request->service_body_content;
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function servcieschagepopup(Request $request){
         $service = Service::findOrFail($request->id);
         $service->name = $request->name;
         $service->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = $request->file('image');
            $iimageName =  "services".time().'.'.$request->file('image')->getclientOriginalName();
            $service->move(public_path('uploads'),$iimageName);
            $service->image = $iimageName;
        }
        else{
            $service->image = $service->image;
        } 

        $service->save();
       return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    public function portfolioupdate(Request $request){
        $page = homepage::findOrFail(1);
        $page->portfolio_content = $request->portfolio_content;
        $page->save();
        return redirect()->back()->with('success', 'Updated Successfully!!!');
    }
    // public function testimonialchange(Request $request){
    //      $testimonials = new Testimonial();
    //       $request->validate([
    //         'name' => 'required',
    //         'description' => 'required',
    //         'image' => 'image|mimes:jpg,png,jpeg|'
    //     ]);
    //     $testimonials->name = $request->name;
    //     $testimonials->description = $request->description;
    //     if(file_exists($request->file('image'))){
    //         $image = "testimonials".time().'.'.$request->file('image')->getclientOriginalName();
    //         $location = public_path('uploads');
    //         $request->file('image')->move($location, $image);
    //         $testimonials->image = $image;
    //     }
    //     else{
    //         $testimonials->image = 'default-thumbnail.png';
    //     }        
    //     $testimonials->save();
    //     return redirect()->back()->with('success', 'Updated Successfully!!!');
    // }
   }