<?php

namespace App\Http\Controllers;

use App\about_us_page;
use Illuminate\Http\Request;

class AboutUsPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $aboutus = about_us_page::all();
        // return view ('dashboard.about-us.index', compact('aboutus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\about_us_page  $about_us_page
     * @return \Illuminate\Http\Response
     */
    public function show(about_us_page $about_us_page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\about_us_page  $about_us_page
     * @return \Illuminate\Http\Response
     */
    public function edit(about_us_page $about_us_page)
    {
        $aboutus  = about_us_page::findOrFail('1');
        return view ('dashboard.about-us.edit', compact('aboutus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\about_us_page  $about_us_page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, about_us_page $about_us_page,$id)
    {
        $request->validate([
            'video' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/mkv,qt | max:500000 ',
        ]);
        $about = about_us_page::findOrFail($id);
        $about->who_are_we = $request->who_are_we;
        $about->video_title = $request->video_title;
        $about->team_description = $request->team_description;
        $about->video_image = $about->video_image;
        if(file_exists($request->file('video'))){
            $file = "about".time().'.'.$request->file('video')->getclientOriginalExtension();
            $location = public_path('uploads/about-us');
            $request->file('video')->move($location, $file);
            $about->video = $file;
        }
        else{
            $about->video =  $about->video;
        }
        if(file_exists($request->file('banner_image'))){
            $image = $request->file('banner_image');
            $imageName =  "banner_image".time().'.'.$request->file('banner_image')->getClientOriginalName();
            $image->move(public_path('uploads/about-us'),$imageName);
            $about->banner_image = $imageName;
        }
        else{
            $about->banner_image = $about->banner_image;
        }
        if(file_exists($request->file('who_are_we_image1'))){
            $image = $request->file('who_are_we_image1');
            $imageName =  "who_are_we_image1".time().'.'.$request->file('who_are_we_image1')->getClientOriginalName();
            $image->move(public_path('uploads/about-us'),$imageName);
            $about->who_are_we_image1 = $imageName;
        }
        else{
            $about->who_are_we_image1 = $about->who_are_we_image1;
        }
        if(file_exists($request->file('who_are_we_image2'))){
            $image = $request->file('who_are_we_image2');
            $imageName =  "who_are_we_image2".time().'.'.$request->file('who_are_we_image2')->getClientOriginalName();
            $image->move(public_path('uploads/about-us'),$imageName);
            $about->who_are_we_image2 = $imageName;
        }
        else{
            $about->who_are_we_image2 = $about->who_are_we_image2;
        }
        $about->save();
        return redirect('/backoffice/about-us')->with('success','About Us Page Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\about_us_page  $about_us_page
     * @return \Illuminate\Http\Response
     */
    public function destroy(about_us_page $about_us_page)
    {
        //
    }
}
