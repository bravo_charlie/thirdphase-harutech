<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class homePage extends Model
{
   protected $fillable = [
    	 'logo_image','phone_number','opening_hours','Social_icon_fb','Social_icon_insta',
    	 'Social_icon_twitter','Social_icon_linkedin','upper_body_image1',
    	 'upper_body_image2','upper_body_content','service_body_content',
    	 'staff_body_content','contact_body_content','lower_body_image1',
    	 'lower_body_content','schedule_content','why_us_content','portfolio_content',
    	 'who_are_we_image1','who_are_we_image2','who_are_we','banner_image'
    ]; 
}
