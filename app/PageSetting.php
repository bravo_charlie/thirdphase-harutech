<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageSetting extends Model
{
    protected $fillable=[
    	'site_title','tagline','site_url','email_address','site_logo','site_favicon','footer_text','footer_visibility','copyright_text','permalink_seo','meta_keywords_seo','meta_description_seo'
    ];
}
