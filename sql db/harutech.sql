-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2020 at 10:34 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `harutech`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us_pages`
--

CREATE TABLE `about_us_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `banner_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `who_are_we` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `who_are_we_image1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `who_are_we_image2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us_pages`
--

INSERT INTO `about_us_pages` (`id`, `banner_image`, `who_are_we`, `who_are_we_image1`, `who_are_we_image2`, `video_title`, `video_image`, `video`, `team_description`, `created_at`, `updated_at`) VALUES
(1, 'banner_image1577348162.page-title-3-1920x305.jpg', '<h2>Who We Are</h2>\r\n\r\n<p><img src=\"/images/line.PNG\" /></p>\r\n\r\n<p><big><strong>Cras ut vestibulum tortor. In in nisi sit amet metus varius pulvinar in vitae ipsum nec mi sollicitudin Fusce turpis massa,</strong></big></p>\r\n\r\n<p>In ante sapien, dapibus luctus aliquet a, accumsan sit amet dolor. Mauris id facilisis dolor. Donec malesuada, est eu dignissim eleifend, est nulla dignissim nisl. Fusce turpis massa, mattis sit.</p>\r\n\r\n<p><strong>Opening Hours</strong></p>\r\n\r\n<ul>\r\n	<li>Monday-Friday:&nbsp;8:00am&ndash;8:00pm</li>\r\n	<li>Saturday:&nbsp;8:00am&ndash;6:00pm</li>\r\n	<li>Sunday:&nbsp;Closed</li>\r\n</ul>\r\n\r\n<p><strong>Our Location</strong></p>\r\n\r\n<ul>\r\n	<li>Address:&nbsp;Washington, USA 6036 Richmond hwy., VA, 2230</li>\r\n	<li>Offices:&nbsp;284-290</li>\r\n</ul>', 'who_are_we_image11577348313.overview-1-470x282.jpg', 'who_are_we_image21577348313.overview-2-470x282.jpg', '<h2>Flowers Can Dance</h2>\r\n\r\n<p><img src=\"/images/white-line.PNG\" /></p>\r\n\r\n<p>In this video, our staff members tell about their work at Haruyosi, how they achieve the best results for their clients every day and more. Click the Play button below to watch this presentation.</p>\r\n\r\n<p>&nbsp;</p>', 'video_image1577348313.parallax-04-1920x1320.jpg', 'about1577790975.mp4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2019-12-26 01:31:30', '2019-12-31 05:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `blogcategories`
--

CREATE TABLE `blogcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogcategories`
--

INSERT INTO `blogcategories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'social media', '2019-09-25 06:04:22', '2019-09-25 06:04:22'),
(2, 'business', '2019-09-25 06:04:39', '2019-09-25 06:04:39'),
(3, 'newsletter', '2019-11-25 15:24:48', '2019-11-25 15:24:48'),
(7, 'demo', '2020-02-05 23:40:36', '2020-02-05 23:40:36');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `cat_id`, `title`, `description`, `f_image`, `i_image`, `created_at`, `updated_at`) VALUES
(1, 2, 'The First Blog', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'blog1569412266.person_1.jpg', 'blog1569412266.nature_small_9.jpg', '2019-09-25 06:06:06', '2019-10-14 10:50:08'),
(2, 1, 'The Second Blog', '<h2>The three greatest things you learn from traveling</h2>\r\n\r\n<p>Like all the great things on earth traveling teaches us by example. Here are some of the most precious lessons I&rsquo;ve learned over the years of traveling.</p>\r\n\r\n<h3>Appreciation of diversity</h3>\r\n\r\n<p>Getting used to an entirely different culture can be challenging. While it&rsquo;s also nice to learn about cultures online or from books, nothing comes close to experiencing cultural diversity in person. You learn to appreciate each and every single one of the differences while you become more culturally fluid.</p>\r\n\r\n<blockquote>\r\n<p>The real voyage of discovery consists not in seeking new landscapes, but having new eyes.</p>\r\n\r\n<p><strong>Marcel Proust</strong></p>\r\n</blockquote>\r\n\r\n<h3>Improvisation</h3>\r\n\r\n<p>Life doesn&#39;t allow us to execute every single plan perfectly. This especially seems to be the case when you travel. You plan it down to every minute with a big checklist; but when it comes to executing it, something always comes up and you&rsquo;re left with your improvising skills. You learn to adapt as you go. Here&rsquo;s how my travel checklist looks now:</p>\r\n\r\n<ul>\r\n	<li>buy the ticket</li>\r\n	<li>start your adventure</li>\r\n</ul>\r\n\r\n<p><img alt=\"Three Monks walking on ancient temple.\" src=\"/assets/images/bg/umbrellas-e935d5c582.jpg\" /></p>\r\n\r\n<p>Leaving your comfort zone might lead you to such beautiful sceneries like this one.</p>\r\n\r\n<h3>Confidence</h3>\r\n\r\n<p>Going to a new place can be quite terrifying. While change and uncertainty makes us scared, traveling teaches us how ridiculous it is to be afraid of something before it happens. The moment you face your fear and see there was nothing to be afraid of, is the moment you discover bliss.</p>', 'blog1569412293.person_7.jpg', 'blog1569412293.nature_small_1.jpg', '2019-09-25 06:06:33', '2019-12-25 00:55:03'),
(3, 3, 'Our New Product Out Now', '<p>We stayed busy launching new features in 2017 and we&#39;re already working on great improvements for 2018. Here&#39;s a sneak peek!&nbsp;</p>\r\n\r\n<p>We stayed busy launching new features in 2017 and we&#39;re already working on great improvements for 2018. Here&#39;s a sneak peek! &nbsp;</p>', 'blog1574673994.Download-Technology-PNG-Transparent.png', 'default-thumbnail.png', '2019-11-25 15:25:07', '2019-11-25 15:26:34'),
(4, 3, 'Check Our Next New Product', '<p>We stayed busy launching new features in 2017 and we&#39;re already working on great improvements for 2018. Here&#39;s a sneak peek!&nbsp;</p>\r\n\r\n<p>We stayed busy launching new features in 2017 and we&#39;re already working on great improvements for 2018. Here&#39;s a sneak peek! &nbsp;</p>\r\n\r\n<p>We stayed busy launching new features in 2017 and we&#39;re already working on great improvements for 2018. Here&#39;s a sneak peek!&nbsp;</p>\r\n\r\n<p>We stayed busy launching new features in 2017 and we&#39;re already working on great improvements for 2018. Here&#39;s a sneak peek! &nbsp;</p>', 'blog1574674031.technology-icon-png-3.png', 'default-thumbnail.png', '2019-11-25 15:27:11', '2019-11-25 15:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `contact_details`
--

CREATE TABLE `contact_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_banner` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_hour` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `map` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_details`
--

INSERT INTO `contact_details` (`id`, `contact_banner`, `banner_text`, `address`, `phone`, `mail`, `opening_hour`, `map`, `created_at`, `updated_at`) VALUES
(1, 'banner1577438241.page-title-4-1920x305.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua incididunt ut labore et dolore magna aliqua.</p>', 'Washington, USA 6036 Richmond hwy., Alexandria, VA, 2230', '+977-1234567890', 'abc@gmail.com', '<ul>\r\n	<li>Mon-Fri: 9 am &ndash; 6 pm</li>\r\n	<li>Saturday: 9 am &ndash; 4 pm</li>\r\n	<li>Sunday: Closed</li>\r\n</ul>', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594417.144356064!2d81.88664297331432!3d28.383842525953103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995e8c77d2e68cf%3A0x34a29abcd0cc86de!2sNepal!5e0!3m2!1sen!2snp!4v1577438127630!5m2!1sen!2snp\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', '2019-12-27 02:10:14', '2019-12-27 03:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE `experiences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(2, 'laravel', 'expertise1569495510.laravellogo.png', '2019-09-26 05:13:30', '2019-09-26 05:13:30'),
(3, 'CSS', 'expertise1569495524.css_logo4.png', '2019-09-26 05:13:44', '2019-09-26 05:13:44'),
(4, 'HTML', 'expertise1569495535.htmlicon.png', '2019-09-26 05:13:55', '2019-09-26 05:13:55'),
(5, 'Illustrator', 'expertise1569495549.illustrator.png', '2019-09-26 05:14:09', '2019-09-26 05:14:09'),
(6, 'Indesign', 'expertise1569495567.indesign.png', '2019-09-26 05:14:27', '2019-09-26 05:14:37'),
(7, 'ionic', 'expertise1569495589.ionic-logo-portrait.png', '2019-09-26 05:14:49', '2019-09-26 05:14:49'),
(8, 'Javascript', 'expertise1569495605.javascripticon.png', '2019-09-26 05:15:05', '2019-09-26 05:15:05'),
(9, 'Photoshop', 'expertise1569495624.photoshop-express.png', '2019-09-26 05:15:24', '2019-09-26 05:15:24'),
(10, 'PHP', 'expertise1569495638.php-file.png', '2019-09-26 05:15:38', '2019-09-26 05:15:38'),
(11, 'Python', 'expertise1569495656.Python-Logo.png', '2019-09-26 05:15:56', '2019-09-26 05:15:56'),
(12, 'Seo', 'expertise1569495668.seo-64.png', '2019-09-26 05:16:08', '2019-09-26 05:16:08'),
(13, 'Vue-js', 'expertise1569495684.vue-js.png', '2019-09-26 05:16:24', '2019-09-26 05:16:24'),
(14, 'Woo commerce', 'expertise1569495697.woocommerce.png', '2019-09-26 05:16:37', '2019-09-26 05:16:37'),
(15, 'Wordpress', 'expertise1569495708.wordpress-64.png', '2019-09-26 05:16:48', '2019-09-26 05:16:48');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` int(10) UNSIGNED DEFAULT NULL,
  `file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `cat_id`, `file`, `url`, `created_at`, `updated_at`) VALUES
(1, 'Newari Collection', 1, 'gallery1575790983.cat3.jpg', '/services', '2019-12-08 01:58:03', '2019-12-08 02:05:13'),
(2, 'Newari Collection', 1, 'gallery1575790983.cat4.jpg', '/', '2019-12-08 01:58:03', '2019-12-08 02:06:53'),
(3, 'Newari Collection', 1, 'gallery1575790983.cat5.jpg', '/services', '2019-12-08 01:58:03', '2019-12-08 02:07:03'),
(4, 'Named Jewellery', 4, 'gallery1577772844.gallery1569412001.services1561439116.png', '/services', '2019-12-31 00:29:04', '2019-12-31 00:29:04'),
(5, 'Named Jewellery', 4, 'gallery1577772844.gallery1569412001.services1561439010.png', '/services', '2019-12-31 00:29:04', '2019-12-31 00:29:04'),
(6, 'Named Jewellery', 4, 'gallery1577772844.gallery1569412002.services1561439166.jpg', '/services', '2019-12-31 00:29:04', '2019-12-31 00:29:04'),
(7, 'Named Jewellery', 4, 'gallery1577772844.gallery1569412002.services1561439205.jpg', '/services', '2019-12-31 00:29:04', '2019-12-31 00:29:04'),
(8, 'Named Jewellery', 4, 'gallery1577772845.gallery1569412003.slider21.jpg', '/services', '2019-12-31 00:29:05', '2019-12-31 00:29:05'),
(9, 'Named Jewellery', 4, 'gallery1577772845.gallery1569412079.final-image.jpg', '/services', '2019-12-31 00:29:05', '2019-12-31 00:29:05'),
(10, 'Named Jewellery', 4, 'gallery1577772846.gallery1569412079.hero_bg_1.jpg', '/services', '2019-12-31 00:29:06', '2019-12-31 00:29:06'),
(11, 'Named Jewellery', 4, 'gallery1577772846.gallery1569412079.hero_bg_2.jpg', '/services', '2019-12-31 00:29:06', '2019-12-31 00:29:06'),
(12, 'Named Jewellery', 4, 'gallery1577772846.gallery1569412079.hero_bg_3.jpg', '/services', '2019-12-31 00:29:06', '2019-12-31 00:29:06'),
(13, 'Named Jewellery', 4, 'gallery1577772846.gallery1569412080.img_1.jpg', '/services', '2019-12-31 00:29:06', '2019-12-31 00:29:06'),
(14, 'Named Jewellery', 4, 'gallery1577772847.gallery1569412080.img_2.jpg', '/services', '2019-12-31 00:29:07', '2019-12-31 00:29:07'),
(15, 'Named Jewellery', 4, 'gallery1577772847.gallery1569412080.img_3.jpg', '/services', '2019-12-31 00:29:07', '2019-12-31 00:29:07'),
(16, 'Gillian Chang', 1, 'gallery1580729273.1.JPG', 'Quia iure sed exerci', '2020-02-03 05:42:53', '2020-02-03 05:42:53'),
(17, 'Gillian Chang', 1, 'gallery1580729274.2.JPG', 'Quia iure sed exerci', '2020-02-03 05:42:54', '2020-02-03 05:42:54');

-- --------------------------------------------------------

--
-- Table structure for table `gallerycategories`
--

CREATE TABLE `gallerycategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallerycategories`
--

INSERT INTO `gallerycategories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Others', '2019-09-25 06:01:16', '2019-10-27 16:06:09'),
(2, 'Service', '2019-09-25 06:02:21', '2019-10-27 16:06:32'),
(3, 'Finance', '2019-10-27 15:57:36', '2019-10-27 16:06:22'),
(4, 'Education', '2019-10-27 15:57:55', '2019-10-27 16:01:24'),
(5, 'E-Commerce', '2019-10-27 16:00:43', '2019-10-27 16:04:58'),
(6, 'Digital Marketing', '2019-10-27 16:02:49', '2019-10-27 16:04:41'),
(7, 'Enterprise', '2019-10-27 16:02:50', '2019-10-27 16:04:21');

-- --------------------------------------------------------

--
-- Table structure for table `home_pages`
--

CREATE TABLE `home_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_hours` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Social_icon_fb` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Social_icon_insta` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Social_icon_twitter` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Social_icon_linkedin` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upper_body_image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upper_body_image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upper_body_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_body_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_body_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_body_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lower_body_image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lower_body_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `why_us_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portfolio_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `home_video` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_pages`
--

INSERT INTO `home_pages` (`id`, `logo_image`, `phone_number`, `opening_hours`, `Social_icon_fb`, `Social_icon_insta`, `Social_icon_twitter`, `Social_icon_linkedin`, `upper_body_image1`, `upper_body_image2`, `upper_body_content`, `service_body_content`, `staff_body_content`, `contact_body_content`, `lower_body_image1`, `lower_body_content`, `schedule_content`, `why_us_content`, `portfolio_content`, `created_at`, `updated_at`, `home_video`) VALUES
(1, 'class=\"icon fa-whatsapp chat-logo\" target=\"_blank\" href=\"https://wa.me/15551234567\"', '1234567890', 'Mon - Fri: 10am - 6pm', 'https://www.facebook.com/admin/', 'https://www.instagram.com/admin/', 'https://www.twitter.com/admin/', 'https://www.linkedin.com/admin/', 'homepage_upper11572604981.home-4-2-328x389.jpg', 'homepage_upper21572604981.home-4-1-310x370.jpg', '<h2>Why...<br />\r\nChoose Us ?</h2>\r\n\r\n<p><img src=\"/images/line.PNG\" /></p>\r\n\r\n<p>Kx Technology is a group of experts committed in fulfilling your digital requirements.</p>\r\n\r\n<p>Our Goal is to meet your expectations and provide you the best solution. We have innovative and passionate team members dedicated toward delivering quality service. We help you get your desired work efficiently. Connect with us today and move towards achieving Excellence.</p>', '<p>We provide a wide range of services for your Flowers<br />\r\nto look clean, attractive, and original.</p>', '<h2>Our Staff</h2>\r\n\r\n<p><img src=\"/images/line.PNG\" /></p>\r\n\r\n<p>Over the years, we have gathered a trusted, talented, and experienced team of Flower technicians and artists.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Beautiful hands give confidence!</h3>', 'why_schedule_image1572764181.home-4-6-498x688.jpg', 'homepages1577786688.mp4', '<h2>Our Expertise</h2>\r\n\r\n<p><img src=\"http://mysedap.net/images/line.png\" /></p>\r\n\r\n<p>We are IT driven company that provide business solutions based on our advanced technology and quality system platform. It is integrated with the artificial intelligence, operational Intelligence, programming, data management and in-house software into one ecosystem. That focuses on delivering high business performance including improvement in business mobility, employee productivity and automating your business operations.</p>\r\n\r\n<p>We transform your ideas and insights into unforgettable design using our developed strategies merged with our user interface (UI), user experience (UX) and graphic design portfolios. We provide solutions based on specific situation and create engaging contents to deliver exceptional user experience enabling technology adoption. We deliver leading edge digital platform that gratifies your expectations and advances your planned executions.</p>', '<h2><strong>Schedule</strong></h2>\r\n\r\n<p><img src=\"/images/line.PNG\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p><strong>Monday-Friday</strong></p>\r\n\r\n	<p>10AM &ndash; 8PM</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p><strong>Saturday</strong></p>\r\n\r\n	<p>10AM &ndash; 6PM</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p><strong>Sunday</strong></p>\r\n\r\n	<p>CLOSED</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>We will be glad to see you anytime at our product.</p>', '<h2>Why Us?</h2>\r\n\r\n<p><img src=\"/images/line.PNG\" /></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Only Top Products</p>\r\n\r\n	<p>OUR TEAMS ARE ALWAYS DEDICATED TOWARDS PRODUCT QUALITY.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>The best Apps</p>\r\n\r\n	<p>WE MAKE SURE THAT OUR APPS MEETS YOUR REQUIREMENT.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Great Feedback</p>\r\n\r\n	<p>EITHER SUPPORT OR FEEDBACK WE ARE ALWAYS HERE.</p>\r\n	</li>\r\n</ul>', '<p>Check out the full portfolio of our works including Flower, Flowering, Flower designs, custom artworks, and more. Everything you see here was performed by our skilled manicurists and pedicurists.</p>', '2019-12-25 05:13:10', '2020-01-14 02:38:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Graphic Designer', 'jobs1574841557.jpg', '<p>Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python&#39;s design philosophy emphasizes code readability with its notable use of significant whitespace.</p>', '2019-09-27 00:09:12', '2019-10-30 09:49:07'),
(2, 'Web Developer', 'jobs1574841311.jpg', '<p>Laravel is a free, open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model&ndash;view&ndash;controller architectural pattern and based on Symfony.</p>', '2019-09-27 00:12:18', '2019-10-30 09:50:46'),
(3, 'Digital Marketing Specialist', 'jobs1569563838.png', '<p>XXX</p>', '2019-10-30 09:27:28', '2019-10-30 09:48:11'),
(4, 'HR Executive', 'jobs15724109988.jpg', '<p>XXX</p>', '2019-10-30 09:29:16', '2019-10-30 09:49:58'),
(5, 'Accounts Executive', 'jobs15724097944.png', '<p>XXX</p>', '2019-10-30 09:29:54', '2019-10-30 09:29:54'),
(6, 'Backend Developer', 'jobs15724111399.png', '<p>XXX</p>', '2019-10-30 09:52:19', '2019-10-30 09:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_details`
--

CREATE TABLE `jobs_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_banner` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_banner` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_banner` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jobs_banner` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jobs_detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs_details`
--

INSERT INTO `jobs_details` (`id`, `service_banner`, `portfolio_banner`, `blog_banner`, `jobs_banner`, `jobs_detail`, `created_at`, `updated_at`) VALUES
(1, 'default-thumbnail.png', 'default-thumbnail.png', 'default-thumbnail.png', 'default-thumbnail.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>', '2019-12-30 04:32:30', '2020-01-14 01:40:07');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `label`, `url`, `order`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Home', '/', 1, NULL, '2019-11-13 18:04:22', '2019-11-13 18:04:22'),
(2, 'About Us', '/about-us', 2, NULL, '2019-11-13 18:04:52', '2019-11-13 18:04:52'),
(3, 'Services', '/services', 3, NULL, '2019-11-13 18:05:27', '2019-11-13 18:05:27'),
(4, 'Portfolio', '/portfolio', 4, NULL, '2019-11-13 18:06:01', '2019-11-13 18:06:01'),
(5, 'News', '/news', 5, NULL, '2019-11-13 18:06:22', '2020-01-15 22:53:21'),
(6, 'Careers', '/careers', 6, NULL, '2019-11-13 18:06:44', '2020-01-15 22:53:32'),
(7, 'Contacts', '/contacts', 7, NULL, '2019-11-13 18:07:07', '2019-11-13 18:07:07'),
(8, 'All Careers', '#', 8, 6, '2019-11-13 18:12:43', '2020-01-03 03:30:59'),
(10, 'Deserunt dolorem ame', 'Eveniet nulla cumqu', 9, 6, '2020-01-06 00:48:05', '2020-01-06 00:48:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_09_070457_create_services_table', 1),
(4, '2019_07_09_072112_create_galleries_table', 1),
(5, '2019_07_09_074934_create_blogcategories_table', 1),
(6, '2019_07_09_075601_create_blogs_table', 1),
(7, '2019_07_09_082710_create_sliders_table', 1),
(8, '2019_07_09_090357_create_testimonials_table', 1),
(9, '2019_08_01_094700_create_staff_table', 1),
(10, '2019_08_05_054250_create_gallerycategories_table', 1),
(11, '2019_08_30_083337_create_experiences_table', 1),
(12, '2019_09_26_114358_create_jobs_table', 1),
(13, '2019_11_01_063000_create_home_pages_table', 1),
(14, '2019_11_13_090226_create_menus_table', 1),
(15, '2019_11_24_112858_create_newsletters_table', 1),
(16, '2019_12_25_105440_add_image_to_users_table', 1),
(17, '2019_12_26_061420_create_about_us_pages_table', 2),
(21, '2019_12_27_065429_create_contact_details_table', 4),
(23, '2019_12_26_095419_create_jobs_details_table', 5),
(27, '2019_12_30_054532_create_page_settings_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'bravocharlie789@gmail.com', '2019-11-24 17:45:04', '2019-11-24 17:45:04'),
(3, 'nepgeeks@gmail.com', '2019-11-26 15:50:36', '2019-11-26 15:50:36'),
(4, 'Sur.shreyas75@gmail.com', '2019-11-26 15:51:09', '2019-11-26 15:51:09'),
(6, 'ashish@nepgeeks.com', '2019-12-23 04:54:06', '2019-12-23 04:54:06'),
(13, 'admin@admin.com', '2020-01-06 05:21:42', '2020-01-06 05:21:42'),
(30, 'azizdulal.ad@gmail.com', '2020-01-06 06:19:32', '2020-01-06 06:19:32');

-- --------------------------------------------------------

--
-- Table structure for table `page_settings`
--

CREATE TABLE `page_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_logo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_favicon` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_visibility` tinyint(1) NOT NULL DEFAULT 0,
  `copyright_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `permalink_seo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords_seo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description_seo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_settings`
--

INSERT INTO `page_settings` (`id`, `site_title`, `tagline`, `site_url`, `email_address`, `site_logo`, `site_favicon`, `footer_text`, `footer_visibility`, `copyright_text`, `permalink_seo`, `meta_keywords_seo`, `meta_description_seo`, `created_at`, `updated_at`) VALUES
(1, 'Demo', 'lorem ipsum', 'demosite.com', 'demosite@mail.com', 'home1577703980.kx-logo.png', 'favicon1578306361.title-logo.png', '<p>Our Goal is to meet your expectations and provide you the best solution. We have innovative and passionate team members dedicated toward delivering quality service. We help you get your desired work efficiently. Connect with us today and move towards achieving Excellence.</p>', 0, '<p>Copyright&copy; 2019 All Rights Reserved by <a href=\"http://www.nepgeeks.com\">Nepgeeks Technology</a></p>', 'demosite', 'demosite', 'demosite', '2019-12-30 03:51:38', '2020-01-06 04:41:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Website Development', 'services1577251731.services1569411710.services1561439116.png', '<p>To help your business attract more visitors and keep them on your site, we offer professional, affordable web design services. We create websites with a comprehensive strategy designed to make customers of your website visitors. Our expert team has built millions of websites and helped thousands of organizations, businesses and individuals build their ideal websites. The website we are building is responsive and mobile-friendly, with millions of devices being browsed. We design your website to compete with your business needs globally. The website we are making also uses to track data that can be used to improve your business, in addition to sharing content information. It is custom mobile and SEO friendly and offers website copywriting, landing page design and optimization.&nbsp; We are a full service and development company that offers a variety of services for web design.</p>', '2019-09-25 05:27:12', '2019-12-24 23:43:51'),
(2, 'Digital Marketing', 'services1577251748.services1569411621.services1561439166.jpg', '<p>A full-service internet marketing company that offers innovative web marketing solutions across the globe to small, medium to large companies. Helping businesses increase sales, build leads, and expand market share. The goal is to provide our customers with a unique and highly effective online presence. With more than a million leads for customers, we become a leading force in redesigning the web.</p>\r\n\r\n<p>We use social media marketing, search marketing and email marketing to promote products and services by leveraging online marketing tactics. Our digital marketing is more cost-effective, provides conversion, generates better revenue, facilitates interaction, cater mobile consumer, and much more. Through digital marketing, we position our clients to become influencers. Our skilled team of social marketers create, manage and deliver top performing social media campaigns for your business.</p>', '2019-09-25 05:55:21', '2019-12-24 23:44:08'),
(3, 'Domain & Hosting', 'services1577251770.services1569411710.services1561439116.png', '<p>Our web hosting works on any web server. A service that make your website available on the internet.&nbsp; Deliver the best tools and server space to get your site hosted, live and equipped for visitors. Crafted with our speed, unparalleled security and reliable web hosting service. Consider as one of the finest domain hosting services that DNS hosting, email hosting and web hosting are offered. Our approach is to provide the latest web hosting trends as a hosting provider, including cloud hosting, new authentication tools, automated backups, advance website builders, new hosting hardware, and eco-friendly hosting for your website.</p>', '2019-09-25 05:55:46', '2019-12-24 23:44:30'),
(4, 'Advisory', 'services1577251810.services1569411763.services1561439010.png', '<p>Our consulting service focuses on helping customers improve their performance and effectively manage their risk. We are working to address specific circumstances and make strategy for them. These circumstances present a vast array of challenges and provide an opportunity to have a serious impact on the businesses of our customers and their own careers. Thus, we offer solution whether its finance, supply chain and customer management.</p>\r\n\r\n<p>We aim to provide integrated approach across a wide range of industries on all types of customers. Using innovative methods to meet the ever-changing needs of businesses. Our services are designed to reflect the priorities of our customers. Aim to help transform their businesses by working with them whenever transactions are planned or underway. Our consultancy is the fastest growing service line of professionals serving around the world. Our success is based on a mix of our dedicated professionals, their expertise and the collective commitment of the company.</p>', '2019-09-25 05:57:43', '2019-12-24 23:45:10'),
(5, 'Graphic Design', 'services1577251873.services1572411395.04 - Designer1.jpg', '<p>From logo designs, re-branding strategies and marketing materials, we offer a wide range of branding and graphic design service. Making sure our customers build brand identity that will direct them to the top. Our skilled professional team will work closely with our client to understand their business &#39; key message. Specializing in creating eye catching and professional designs to foster your brand and connect to your target audience. We offer a comprehensive graphic design service that covers all areas of art. The graphic designs that we made are known and praised by many around the world. It is user friendly and provides user interface design, publishing design, movement design, and much more. The graphic we structure uses aesthetics and technical requirements when designing a format for your brand. To meet your design requirements, we offer custom designs and complete solutions.</p>', '2019-10-27 16:58:51', '2019-12-24 23:46:13'),
(6, 'Mobile Apps Development', 'services1577251890.services1572406730.03 - Mobile.png', '<p>Among several marketing methodologies, having an app for business promotion has been determined to be the most effective marketing tool as it drives traffics and revenue to the site in a bulk<strong>.&nbsp;</strong>We provides app development services to its valuable clients. We have been building an application that is compatible to all operating systems whether it is IOS, Android, Windows or others. Our services include simple table based app, database app and Games. We also help in promoting apps in Google and other platforms. We assure you that we have no unreasonable fees and the price depends on the features of the application.&nbsp;</p>', '2019-10-30 08:38:50', '2019-12-24 23:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `image`, `title`, `title_1`, `description`, `created_at`, `updated_at`) VALUES
(1, 'tech1', 'Slider1569408219.Slider1558599745.png', 'Ship better software,', 'faster & Reliable', '<h3><samp>Everything your team needs to build a great software.</samp></h3>', '2019-09-25 04:48:49', '2019-09-25 04:58:39'),
(2, 'Aline Elliott', 'Slider1569408201.slider21.jpg', 'WE HELP YOU GET MORE TRAFFIC,', 'LEADS & SALES', '<h3><samp>Toronto&rsquo;s Search Engine Experts&nbsp;</samp><samp>In Malaysia.</samp></h3>', '2019-09-25 04:58:21', '2019-09-25 05:00:26');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `name`, `designation`, `image`, `description`, `facebook_link`, `twitter_link`, `instagram_link`, `mail`, `created_at`, `updated_at`) VALUES
(1, 'Lionel Parsons', 'Developer', 'staffs1569412418.person_3.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Dolore perferendis t', 'Fugit maxime dolore', 'Consequuntur alias r', 'ratiw@mailinator.net', '2019-09-25 06:08:38', '2019-10-02 13:52:17'),
(2, 'Genevieve Deleon', 'Marketting Manager', 'staffs1569412436.person_1.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Ut maiores mollitia', 'Harum et eum officia', 'Aliqua Tenetur nemo', 'qehix@mailinator.com', '2019-09-25 06:08:56', '2019-10-02 13:52:37'),
(3, 'Ariel Crane', 'Designer', 'staffs1569412456.img_5.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Recusandae Adipisci', 'Voluptas tempora con', 'Ratione ipsum dignis', 'lozidicegu@mailinator.net', '2019-09-25 06:09:16', '2019-10-02 13:52:55'),
(4, 'Beck Workman', 'Quinn Lawson', 'default-thumbnail.png', '<p>qwertuioplkmgfdsazxcvbnm</p>', 'Eos ullamco doloremq', 'Rem eos a ex labore', 'Eligendi dolorem vel', 'lyga@mailinator.net', '2020-01-14 00:15:42', '2020-01-14 00:15:42');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Jarrod Conrad', 'testimonials1569412333.person_3.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2019-09-25 06:07:13', '2019-09-25 06:07:13'),
(2, 'Josiah Buckley', 'testimonials1569412348.person_2.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2019-09-25 06:07:28', '2019-09-25 06:07:28'),
(3, 'Kendall Farrell', 'testimonials1569412377.person_7.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2019-09-25 06:07:57', '2019-09-25 06:07:57'),
(4, 'Leroy Francis', 'default-thumbnail.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2020-01-13 23:16:07', '2020-01-14 00:16:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `image`, `role`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$i/Tjx4niPaFM.CK7cAVWtuURP1XIOd5FcAUsgVYMGxaE5w2qFI26u', NULL, '2019-12-25 05:13:10', '2019-12-25 05:58:54', 'user1577274234.ashish c.jpg', NULL),
(14, 'Ashish Dulal', 'ashish@nepgeeks.com', NULL, '$2y$10$T.vxxv.5zs.OJ7HhZucHdODwdiGwKpRw..I/9sePpQA6yntl68ONu', NULL, '2020-01-05 23:34:02', '2020-01-05 23:52:15', 'user1578289035.ashisha.jpg', NULL),
(30, 'Quynn Mccoy', 'cymepe@mailinator.net', NULL, '$2y$10$7/g.G.KVPdWzgg9C6KIROuRCB4CrJwdYv.o.AbV/gSSSlqR7Mw1/q', NULL, '2020-01-14 04:14:42', '2020-01-14 04:14:42', 'admin-image.png', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us_pages`
--
ALTER TABLE `about_us_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogcategories`
--
ALTER TABLE `blogcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_details`
--
ALTER TABLE `contact_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallerycategories`
--
ALTER TABLE `gallerycategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_pages`
--
ALTER TABLE `home_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs_details`
--
ALTER TABLE `jobs_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`) USING BTREE;

--
-- Indexes for table `page_settings`
--
ALTER TABLE `page_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us_pages`
--
ALTER TABLE `about_us_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogcategories`
--
ALTER TABLE `blogcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contact_details`
--
ALTER TABLE `contact_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `gallerycategories`
--
ALTER TABLE `gallerycategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home_pages`
--
ALTER TABLE `home_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jobs_details`
--
ALTER TABLE `jobs_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `page_settings`
--
ALTER TABLE `page_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
